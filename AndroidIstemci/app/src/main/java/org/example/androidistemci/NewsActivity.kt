package org.example.androidistemci

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Message
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_news.*
import org.example.androidistemci.adapters.NewsAdapter
import org.example.androidistemci.adapters.OnCheckBoxClickListener
import org.example.androidistemci.adapters.OnItemClickListener
import org.example.androidistemci.adapters.TypesAdapter
import org.example.androidistemci.data.News
import org.example.androidistemci.services.NewsNotificationService
import org.json.JSONArray
import org.json.JSONObject

class NewsActivity : AppCompatActivity(), OnItemClickListener, OnCheckBoxClickListener {
    private val newsList = mutableListOf<News>()
    private val selectedNews = mutableListOf<News>()
    private val newsTypes = mutableListOf<String>()
    private val selectedTypes = newsTypes.toMutableList()
    private val newsAdapter = NewsAdapter(selectedNews)
    private val typesAdapter = TypesAdapter(newsTypes)
    private lateinit var newsHandler: NewsHandler
    private var notificationService: NewsNotificationService? = null
    private var isBound = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news)

        newsAdapter.setOnItemClickListener(this)
        typesAdapter.setOnItemClickListener(this)

        recyclerViewNews.apply {
            setHasFixedSize(false)
            layoutManager = LinearLayoutManager(this@NewsActivity)
            adapter = newsAdapter
        }

        listViewTypes.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@NewsActivity)
            adapter = typesAdapter
        }

        val queue = Volley.newRequestQueue(this)
        val newsUrl = "${getString(R.string.url)}/api/news"
        val newsRequest = StringRequest(Request.Method.GET, newsUrl,
                responseListenerGetNews,
                errorListenerGetNews)
        val newsTypesUrl = "${getString(R.string.url)}/api/news-types"
        val newsTypesRequest = StringRequest(Request.Method.GET, newsTypesUrl,
                responseListenerGetNewsTypes,
                errorListenerGetNewsTypes)

        queue.apply {
            add(newsRequest)
            add(newsTypesRequest)
        }

        newsHandler = NewsHandler(onAddNewsListener)
    }

    override fun onDestroy() {
        super.onDestroy()

        if (isBound) {
            unbindService(serviceConnection)
            isBound = false
        }
    }

    private val responseListenerGetNews = Response.Listener<String> {
        // Add all messages to the list in the response
        val gson = Gson()

        val jsonArray = JSONArray(it)
        for (jsonObject in jsonArray) {
            if (jsonObject !is JSONObject) continue

            val news: News = gson.fromJson(jsonObject.toString(), News::class.java)
            selectedNews.add(news)
            newsList.add(news)
        }

        this@NewsActivity.runOnUiThread {
            newsAdapter.notifyDataSetChanged()
        }

        startService(Intent(this, NewsNotificationService::class.java))
        bindService(Intent(this, NewsNotificationService::class.java), serviceConnection, Context.BIND_AUTO_CREATE)
        isBound = true
    }

    private val errorListenerGetNews = Response.ErrorListener {
        it.printStackTrace()
        Toast.makeText(this@NewsActivity, R.string.server_isnt_responding, Toast.LENGTH_SHORT).show()
    }

    private val responseListenerGetNewsTypes = Response.Listener<String> {
        val jsonArray = JSONArray(it)
        for (jsonObject in jsonArray) {
            if (jsonObject !is JSONObject) continue

            val newsType = jsonObject.getString("name")
            newsTypes.add(newsType)
            selectedTypes.add(newsType)
        }

        this@NewsActivity.runOnUiThread {
            typesAdapter.notifyDataSetChanged()
        }
    }

    private val errorListenerGetNewsTypes = Response.ErrorListener {
        it.printStackTrace()
        Toast.makeText(this@NewsActivity, R.string.server_isnt_responding, Toast.LENGTH_SHORT).show()
    }

    override fun onItemClick(position: Int, view: View) {
        val intent = Intent(this@NewsActivity, DetailsActivity::class.java)

        val news = selectedNews[position]
        intent.apply {
            putExtra(News.EXTRA_ID, news._id)
            putExtra(News.EXTRA_AUTHOR, news.author)
            putExtra(News.EXTRA_TITLE, news.title)
            putExtra(News.EXTRA_BODY, news.body)
            putExtra(News.EXTRA_TYPE, news.type)
            putExtra(News.EXTRA_IMAGE, news.image)
            putExtra(News.EXTRA_PUBLICATION_DATE, news.publicationDate)
            putExtra(News.EXTRA_CREATED_AT, news.createdAt)
            putExtra(News.EXTRA_UPDATED_AT, news.updatedAt)
        }

        startActivity(intent)
    }

    private fun refreshSelectedNews() {
        selectedNews.clear()
        newsList.forEach {
            if (selectedTypes.contains(it.type))
                selectedNews.add(it)
        }

        this@NewsActivity.runOnUiThread {
            newsAdapter.notifyDataSetChanged()
        }
    }

    override fun onCheckBoxClick(position: Int, view: View) {
        val type = newsTypes[position]

        if (selectedTypes.contains(type))
            selectedTypes.remove(type)
        else
            selectedTypes.add(type)

        refreshSelectedNews()
    }

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {
            notificationService = null
        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            notificationService = (service as NewsNotificationService.LocalBinder).getInstance()
            notificationService?.handler = newsHandler
        }
    }

    private val onAddNewsListener = object : OnAddNewsListener {
        override fun onAddNews(news: News) {
            newsList.add(news)
            refreshSelectedNews()
        }
    }

    interface OnAddNewsListener {
        fun onAddNews(news: News)
    }

    class NewsHandler(private val onAddNewsListener: OnAddNewsListener) : Handler() {
        override fun handleMessage(msg: Message?) {
            super.handleMessage(msg)

            val bundle = msg?.data ?: return

            onAddNewsListener.onAddNews(
                    bundle.run {
                        @Suppress("UNCHECKED_CAST")
                        News(
                                getString(News.EXTRA_AUTHOR) ?: return,
                                getString(News.EXTRA_TITLE) ?: return,
                                getString(News.EXTRA_BODY) ?: return,
                                getString(News.EXTRA_TYPE) ?: return,
                                getSerializable(News.EXTRA_IMAGE) as? Array<Byte> ?: return,
                                getLong(News.EXTRA_PUBLICATION_DATE),
                                getLong(News.EXTRA_CREATED_AT),
                                getLong(News.EXTRA_UPDATED_AT),
                                getString(News.EXTRA_ID) ?: return,
                                getInt(News.EXTRA_LIKES),
                                getInt(News.EXTRA_DISLIKES),
                                getInt(News.EXTRA_VIEWS)
                        )
                    })
        }
    }
}
