package org.example.androidistemci.adapters

import android.view.View

interface OnCheckBoxClickListener {
    fun onCheckBoxClick(position: Int, view: View)
}