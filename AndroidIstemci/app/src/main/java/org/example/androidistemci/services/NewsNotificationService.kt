package org.example.androidistemci.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.*
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.util.Log
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import org.example.androidistemci.DetailsActivity
import org.example.androidistemci.R
import org.example.androidistemci.data.News
import org.java_websocket.client.WebSocketClient
import org.java_websocket.handshake.ServerHandshake
import java.net.URI
import java.util.*

class NewsNotificationService : Service() {

    companion object {
        private const val TAG = "NewsNotificationService"
    }

    var handler = Handler()
    private lateinit var webSocketClient: WebSocketClient
    private lateinit var timer: Timer
    private lateinit var queue: RequestQueue
    private lateinit var url: String
    private val iBinder = LocalBinder()

    override fun onBind(intent: Intent?): IBinder? {
        return iBinder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(TAG, "onStartCommand")
        super.onStartCommand(intent, flags, startId)

        return START_STICKY
    }

    override fun onCreate() {
        Log.d(TAG, "onCreate")

        connectWebSocket()
        queue = Volley.newRequestQueue(this)
        url = "${getString(R.string.url)}/api/news/check"

        createNotificationChannel()
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy")

        //stop the timer, if it's not already null
        timer.cancel()
    }

    private fun connectWebSocket() {
        webSocketClient = object : WebSocketClient(URI(getString(R.string.ws_url))) {
            override fun onOpen(handshakedata: ServerHandshake?) {
                Log.i(TAG, "WebSocket: onOpen")
            }

            override fun onClose(code: Int, reason: String?, remote: Boolean) {
                Log.i(TAG, "WebSocket: onClose\tReason: $reason")
            }

            override fun onMessage(message: String?) {
                Log.i(TAG, "WebSocket: onMessage\tMessage: $message")
                val gson = Gson()
                val news = gson.fromJson(message, News::class.java)
                val intent = Intent(this@NewsNotificationService, DetailsActivity::class.java)
                intent.apply {
                    putExtra(News.EXTRA_ID, news._id)
                    putExtra(News.EXTRA_AUTHOR, news.author)
                    putExtra(News.EXTRA_TITLE, news.title)
                    putExtra(News.EXTRA_BODY, news.body)
                    putExtra(News.EXTRA_TYPE, news.type)
                    putExtra(News.EXTRA_IMAGE, news.image)
                    putExtra(News.EXTRA_PUBLICATION_DATE, news.publicationDate)
                    putExtra(News.EXTRA_CREATED_AT, news.createdAt)
                    putExtra(News.EXTRA_UPDATED_AT, news.updatedAt)
                    putExtra(News.EXTRA_LIKES, news.likes)
                    putExtra(News.EXTRA_DISLIKES, news.dislikes)
                    putExtra(News.EXTRA_VIEWS, news.views)
                }

                val pendingIntent = PendingIntent.getActivity(this@NewsNotificationService, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT)
                val builder = NotificationCompat.Builder(applicationContext, TAG)
                        .setSmallIcon(R.drawable.ic_newspaper)
                        .setContentTitle(getString(R.string.there_are_new_news))
                        .setContentText(news.title)
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)

                with(NotificationManagerCompat.from(applicationContext)) {
                    // notificationId is a unique int for each notification that you must define
                    notify(0, builder.build())
                }

                val msg = handler.obtainMessage()
                val bundle = Bundle()
                bundle.apply {
                    putString(News.EXTRA_ID, news._id)
                    putString(News.EXTRA_AUTHOR, news.author)
                    putString(News.EXTRA_TITLE, news.title)
                    putString(News.EXTRA_BODY, news.body)
                    putString(News.EXTRA_TYPE, news.type)
                    putSerializable(News.EXTRA_IMAGE, news.image)
                    putLong(News.EXTRA_PUBLICATION_DATE, news.publicationDate)
                    putLong(News.EXTRA_CREATED_AT, news.createdAt)
                    putLong(News.EXTRA_UPDATED_AT, news.updatedAt)
                    putInt(News.EXTRA_LIKES, news.likes)
                    putInt(News.EXTRA_DISLIKES, news.dislikes)
                    putInt(News.EXTRA_VIEWS, news.views)
                }
                msg.data = bundle

                handler.sendMessage(msg)
            }

            override fun onError(ex: Exception?) {
                Log.e(TAG, ex?.message ?: "WebSocket: onError")
            }
        }
        webSocketClient.connect()
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = TAG
            val descriptionText = TAG
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(TAG, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    inner class LocalBinder : Binder() {
        fun getInstance(): NewsNotificationService {
            return this@NewsNotificationService
        }
    }
}